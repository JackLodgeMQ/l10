#ifndef SENSORS_HPP
#define SENSORS_HPP
#endif

#include "Arduino.h"
//#include "Motor.hpp"

class Sensors{
    private:
        // Variables
        int level;
        //Motor myMotor;
        int threshold;
        // Limit switches
        bool bottomLimit;
        bool topLimit;

    public:
        bool limitReached;
        Sensors();
        int checkLevel();
        void manualSetLevel(int level);
        void updateLevel(int floor1, int floor2, int floor3, int floor4, int floor5, int bot, int top);
        void limit(int bot, int top);
};
