//https://i.pinimg.com/originals/d7/da/58/d7da5887c8c1fe662f4e42413696df69.png
//Arduino pin map
    //Analong pins
    #define floorLevel1 (54)//A0
    #define floorLevel2 (55)//malfunctioning
    #define floorLevel3 (56)
    #define floorLevel4 (57)
    #define floorLevel5 (58)

    // Digital pins
    #define encoderChannelA (2) // Use 2, 3 , 18, 19, 20 and 21
    #define encoderChannelB (3) // Use 2, 3, 18, 19, 20 and 21
    #define motorPWM (4)
    #define doorPinL (7)
    #define doorPinR (8)
    #define bottomSwitch (52)
    #define topSwitch (53)

    //Serial pins
    //#define controlBoxA (9) 
    //#define controlBoxB (10)

    //Buttons
    #define buttonL1 (38)
    #define buttonL2 (32)//not working properly
    #define buttonL3 (40)
    #define buttonL4 (44)
    #define buttonL5 (36)
    #define buttonOpen (42)//not working
    #define buttonClose (30)
    #define buttonE (34)
