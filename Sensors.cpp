#include "Arduino.h"
#include "Sensors.hpp"


Sensors::Sensors(){
    this->threshold = 500;
    limitReached = false;
}

void Sensors::manualSetLevel(int level){
  if(level < 6 && level>=0){
    this->level = level;
  }
}

int Sensors::checkLevel(){
    return this->level;
}

void Sensors::updateLevel(int floor1, int floor2, int floor3, int floor4, int floor5, int bot, int top){
    if(floor1 < threshold){
        this->level = 1;
        
    } else if(floor2 < threshold){
        this->level = 2;
        
    } else if(floor3 < threshold){
        this->level = 3;
        
    }else if(floor4 < threshold){
        this->level = 4;
        
    }else if(floor5 < threshold){
        this->level = 5;
        
    }
    limit(bot, top);
}

void Sensors::limit(int bot, int top){
    if(bot == HIGH){
        limitReached = true;
        this->level = 0;
    }

    if(top == HIGH){
        limitReached = true;
        this->level = 6;
    }
}
