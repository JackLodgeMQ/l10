#include <LiquidCrystal_I2C.h>
#include "Arduino.h"
#include "Sensors.hpp"
#include "Pins.hpp"
#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h> //Download Here: https://www.pjrc.com/teensy/td_download.html NOTE: Only install "Encoder"!
#include <Servo.h>

LiquidCrystal_I2C lcd(0x27,16,2);

// the different commands that can be run by the controlbox and state machine
enum Commands{
    IDLE,
    DOOR_OPEN,
    DOOR_CLOSE,
    MOVE_TO_FLOOR,
    EMERGENCY_STOP,
    CALLIBRATE
};

Commands command;
Commands prevCommand;
boolean eStop;

//Sensor values
Sensors sensorInfo;
int goalFloor;
int currentFloor;
int startFloor;

//Serial data
char inputData;

// Door Values
Servo doorMotorL;
Servo doorMotorR;

//Lift Motor values
Servo liftMotor;
Encoder liftMotorEncoder(encoderChannelA, encoderChannelB);
int liftMotorSpeed;

// debouncing variable
boolean btnPressed = false;

void setup()
{
  Serial.begin(9600);
  Serial1.begin(9600);
  pinMode(buttonL1, INPUT);
  pinMode(buttonL2, INPUT);
  pinMode(buttonL3, INPUT);
  pinMode(buttonL4, INPUT);
  pinMode(buttonL5, INPUT);
  pinMode(buttonOpen, INPUT);
  pinMode(buttonClose, INPUT);
  pinMode(buttonE, INPUT);
  
  lcd.begin();
  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("    LIFT 10     ");
  
  command = Commands::CALLIBRATE;
  
  //attach motor and servo motors
  liftMotor.attach(motorPWM);
  //doorMotorL.attach(doorPinL);
  //doorMotorR.attach(doorPinR);
  sensorInfo = Sensors();
  
   currentFloor = 0;
   eStop = false;
    
}

void loop()
{
    //do not remove lines 58 - 65
    /*sensorInfo.updateLevel(analogRead(floorLevel1),
                           analogRead(floorLevel2),
                           analogRead(floorLevel3),
                           analogRead(floorLevel4),
                           analogRead(floorLevel5),
                           digitalRead(bottomSwitch),
                           digitalRead(topSwitch));*/
    sensorInfo.updateLevel(analogRead(floorLevel1),
                           analogRead(floorLevel2),
                           analogRead(floorLevel3),
                           analogRead(floorLevel4),
                           analogRead(floorLevel5),
                           digitalRead(bottomSwitch),
                           digitalRead(topSwitch));
    if(currentFloor != sensorInfo.checkLevel()){
        currentFloor = sensorInfo.checkLevel();
        liftMotorEncoder.write(0);
        /*if(currentFloor == 1) Serial1.println("a");
        else if(currentFloor == 2) Serial1.println("b");
        else if(currentFloor == 3) Serial1.println("c");
        else if(currentFloor == 4) Serial1.println("d");
        else if(currentFloor == 5) Serial1.println("e");
    */
    }

   if(Serial1.available()){
        inputData = Serial1.read();
        switch(inputData){
            case 'f'://Emergency Stop
              /*if(eStop == false){
                prevCommand = command;
                eStop = true;
              }else{
                eStop = false;
              }*/
              command = Commands::EMERGENCY_STOP;
            break;
            
            case 'a':
              command = Commands::MOVE_TO_FLOOR;
              goalFloor = 1;
            break;
            
            case 'b':
              command = Commands::MOVE_TO_FLOOR;
              goalFloor = 2;
            break;
            
            case 'c':
              command = Commands::MOVE_TO_FLOOR;
              goalFloor = 3;
            break;
            
            case 'd':
              command = Commands::MOVE_TO_FLOOR;
              goalFloor = 4;
            break;
            
            case 'e':
              command = Commands::MOVE_TO_FLOOR;
              goalFloor = 5;
            break;

            case 'g':
              lcd.setCursor(0,1);
              lcd.print(" DOOR OPEN ");
              command = Commands::DOOR_OPEN;
            break;

            case 'h':
              lcd.setCursor(0,1);
              lcd.print(" DOOR CLOSE ");
              command = Commands::DOOR_CLOSE;
            break;
        }
        //btnPressed = true;
    }/*else{
          btnPressed = false;
    }*/
    
    if(eStop == true || sensorInfo.limitReached == true){
      command = Commands::EMERGENCY_STOP;
    }
    //setMotorSpeed(15);
    updateLCD();
    readButtons();
    performTask(); 
}

void readButtons(){
    if(digitalRead(buttonOpen)==HIGH && btnPressed != true){
      btnPressed = true;
      lcd.setCursor(0,1);
      lcd.print(" DOOR OPEN ");
      command = Commands::DOOR_OPEN;  
      //eStop = true;
    }
    else if(digitalRead(buttonClose)==HIGH && btnPressed != true){
      btnPressed = true;
      lcd.setCursor(0,1);
        lcd.print(" DOOR CLOSE ");
        command = Commands::DOOR_CLOSE;
      //eStop = true;
    }
    else if(digitalRead(buttonE)==HIGH && btnPressed != true){
      if(eStop)
      {
        command = Commands::CALLIBRATE;
      }
      else
      {
        command = Commands::EMERGENCY_STOP;
      }
    }
    else if((digitalRead(buttonL1)==HIGH && btnPressed != true))
    {
      btnPressed = true;
  //    lcd.setCursor(0,1);
  //      lcd.print("    LEVEL 1     ");
      command = Commands::MOVE_TO_FLOOR;
      goalFloor = 1;
    }
    if((digitalRead(buttonL2)==HIGH) && btnPressed != true)
    {
  //    lcd.setCursor(0,1);
  //      lcd.print("    LEVEL 2     ");
      btnPressed = true;
      command = Commands::MOVE_TO_FLOOR;
      goalFloor = 2;
    }
    else if((digitalRead(buttonL3)==HIGH) && btnPressed != true)
    {
  //    lcd.setCursor(0,1);
  //      lcd.print("    LEVEL 3     ");
      btnPressed = true;
      command = Commands::MOVE_TO_FLOOR;
      goalFloor = 3;
    }
    else if((digitalRead(buttonL4)==HIGH) && btnPressed != true)
    {
  //    lcd.setCursor(0,1);
  //      lcd.print("    LEVEL 4     ");
      btnPressed = true;
      command = Commands::MOVE_TO_FLOOR;
      goalFloor = 4;
    }
    else if((digitalRead(buttonL5)==HIGH) && btnPressed != true)
    {
  //    lcd.setCursor(0,1);
  //    lcd.print("    LEVEL 5     ");
      btnPressed = true;
      command = Commands::MOVE_TO_FLOOR;
      goalFloor = 5;
    }else{
      if(digitalRead(buttonOpen) ==0 && digitalRead(buttonClose) ==0 && digitalRead(buttonE) ==0 && digitalRead(buttonL1) ==0 && digitalRead(buttonL2) ==0 && digitalRead(buttonL3) ==0 && digitalRead(buttonL4) ==0 && digitalRead(buttonL5) == 0){
         btnPressed = false;
      }
    }
}

void updateLCD(){
  if(command == Commands::EMERGENCY_STOP){
      lcd.setCursor(0,1);
      lcd.print(" MANUAL E-STOP ");
      lcd.setCursor(15,1);
      lcd.print(sensorInfo.checkLevel());
  }else{
    switch(currentFloor)
    {
      case 1: 
        lcd.setCursor(0,1);
        lcd.print("    LEVEL 1     ");
        break;
      
      case 2:
        lcd.setCursor(0,1);
        lcd.print("    LEVEL 2     ");
        break;
      
      case 3:
        lcd.setCursor(0,1);
        lcd.print("    LEVEL 3     ");
        break;
      
      case 4:
        lcd.setCursor(0,1);
        lcd.print("    LEVEL 4     ");
        break;
      
      case 5:
        lcd.setCursor(0,1);
        lcd.print("    LEVEL 5     ");
        break;
        
      default:
      break;      
    }
  }
}

void performTask(){
    int t;
    switch(command){
        case DOOR_OPEN:
            // open door command
            // after 1 second the command will report completion allowing the doors time to open
            openDoor();
            t = millis();
            if(t%1000 == 0){
                reportCompletion();
            }
            break;
        case DOOR_CLOSE:
            // close door command
            // after 1 second the command will report completion allowing time for the door to close
            closeDoor();
            t = millis();
            if(t%1000 == 0){
                reportCompletion();
            }
            break;
        case MOVE_TO_FLOOR:
            /// assume goal floor has been updated for this to run
            if(goalFloor == currentFloor){
                reportCompletion();
                setMotorSpeed(0);
            }else{
                setMotorSpeed(ramp(calcDistToGoal(currentFloor, 
                                                  goalFloor,
                                                  -1*getMotorPos())));
            }
            
            break;
        case EMERGENCY_STOP:
        //requires restart
            eStop = true;
            setMotorSpeed(0);
            currentFloor = 0;
            startFloor = 0;
            goalFloor = 0;
            sensorInfo.manualSetLevel(0);
            break;
        case IDLE://do nothing until command is changed
          setMotorSpeed(0);
          break;
        case CALLIBRATE:
        /********************************************************************
         *  calibration of carriage
         *  on start up move to  the closes floor
         *  assume at top and move down till carriage triggers a floor sensor
         ********************************************************************/
          if(currentFloor <=0){
              lcd.setCursor(0,1);
              lcd.print("Initial");
              sensorInfo.updateLevel(analogRead(floorLevel1),
                                   analogRead(floorLevel2),
                                   analogRead(floorLevel3),
                                   analogRead(floorLevel4),
                                   analogRead(floorLevel5),
                                   digitalRead(bottomSwitch),
                                   digitalRead(topSwitch));
              lcd.setCursor(9,1);
              lcd.print(sensorInfo.checkLevel());
              if(sensorInfo.checkLevel() != currentFloor){
                currentFloor = sensorInfo.checkLevel();
              }
              else{
                setMotorSpeed(20);
              }
          }else{
            eStop = false;
            command = Commands::IDLE;
            goalFloor = currentFloor;
            startFloor = currentFloor;
            liftMotorEncoder.write(0);
         }
          break;
    }
}

void reportCompletion(){
    command = Commands::IDLE;
}


//aim for mvp
long calcGlobDist(int curr, long encoDist){
    int globalHeight = getHeight(curr) + encoDist;
    return globalHeight;
}

long calcDistToGoal(int curr, int goal, long encoDist){
    return getHeight(goal)-(getHeight(curr)+encoDist);
}

long getHeight(int floor){
    // to do edit height for actual distance
    int levelHeight = 5900;// reference to encoder ticks
    return (floor)*levelHeight;
}

 // minimum start speed is 20% 
long ramp(long distToGo){
    if(getMotorSpeed() == 0 && currentFloor != goalFloor){
      startFloor = currentFloor;
      Serial.print("Elevator Started from floor: ");
      Serial.println(startFloor);
    }

    int overallDist = (float)calcDistToGoal(startFloor, goalFloor, 0);
    int newSpeed;
   
    //improved version reduced speed and ramp up and down are done over roughly the same period of time regardless of dist to travel
    int distFromStart = overallDist-distToGo;
    // from start to goal initLevelPercent will go from 0% to 100% then to at most 500% for going from floor 1 to 5 or vice versa
    int initLevelPercent = (abs((float)distFromStart)/abs((float)calcDistToGoal(startFloor, startFloor+1, 0)))*100;
    //From start to Goal finalLevelPercent will go from at most 500% to 0% 
    int finalLevelPercent = (abs((float)distToGo)/abs((float)calcDistToGoal(goalFloor-1, goalFloor, 0)))*100;
    
    
    //50% max speed
    if(initLevelPercent < 20){ // motor ramp up to max speed
        newSpeed = 1.5*initLevelPercent + 20;
    }else if(finalLevelPercent < 10){// from 90% to 100% reduce speed from 25% to 15% 
        newSpeed = finalLevelPercent + 15;
    }else if(finalLevelPercent < 30){// from 70% to 90% reduce speed from 50% speed to 25% speed
        newSpeed = 1.25*finalLevelPercent+12; 
    }else{  //will apply from 20% path completion to 70% path completion travel at 70% speed
        newSpeed = 50;
    }

    /*
    //75% max speed
    if(initLevelPercent < 20){ // motor ramp up to max speed
        newSpeed = 2.75*initLevelPercent + 20;
    }else if(finalLevelPercent < 10){// from 90% to 100% reduce speed from 25% to 15% 
        newSpeed = finalLevelPercent + 15;
    }else if(finalLevelPercent < 30){// from 70% to 90% reduce speed from 50% speed to 25% speed
        newSpeed = 2.5*finalLevelPercent; 
    }else{  //will apply from 20% path completion to 70% path completion travel at 70% speed
        newSpeed = 75;
    }*/
    
    /*
    //100% max speed
    if(initLevelPercent < 20){ // motor ramp up to max speed
        newSpeed = 4*initLevelPercent + 20;
    }else if(finalLevelPercent < 10){// from 90% to 100% reduce speed from 25% to 15% 
        newSpeed = finalLevelPercent + 15;
    }else if(finalLevelPercent < 30){// from 70% to 90% reduce speed from 100% speed to 25% speed
        newSpeed = 3.75*finalLevelPercent+12.5; 
    }else{  //will apply from 20% path completion to 70% path completion travel at 70% speed
        newSpeed = 100;
    }*/
    
    //Serial.println(percentComplete);
    //give motor direction from distance to travel
    if(distToGo >= 0){
        return -newSpeed;
    }else{
        return newSpeed;
    }
}


// Motor does not run properly in it's own class, motor when operated in class is very jittery
void setMotorSpeed(int newSpeed){
    if(newSpeed != liftMotorSpeed){
      liftMotorSpeed = newSpeed;
      int servoVal = map(newSpeed, -100, 100, 0, 180);
      liftMotor.write(servoVal);
      Serial.println(liftMotorSpeed);
    }
}

int getMotorSpeed(){
  return liftMotorSpeed;
}

long getMotorPos(){
  return liftMotorEncoder.read();
}

//after mvp
// P I speed control
int piSpeed(){

    return 0;
}

//Door functions
void openDoor(){
  moveDoor(90);
}

void closeDoor(){
    moveDoor(0);
}

void moveDoor(int pos){
    //angle between 0 to 180
    doorMotorL.write(pos);
    doorMotorR.write(180-pos);
}
